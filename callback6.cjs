

const boards = require('./data/boards_1.json')

const lists = require('./data/lists_1.json')

const callback1 = require('./callback1.cjs')

const callback2 = require('./callback2.cjs')

const callback3 = require('./callback3.cjs')

function callback6( board_name, final_callback ){

    setTimeout(() => {

        let get_board_id = boards.filter( (board) => {
            
            if(board.name == board_name){

                return board;

            }

        } )

        /// For finding information about a board id of given board name.
        if(!get_board_id){

            final_callback("No board found with this name", null)

        }else{

            let board_id = get_board_id[0].id;

            callback1(board_id, (err, board_data) => {

                if(err){

                    final_callback(err, null)
                }
                else{
                    final_callback(null, board_data)

                    // for finding all the lists of a board it
                    callback2( board_id, (err, board_data) => {
                        
                        if(err){

                            final_callback(err, null)

                        }else{

                            final_callback(null, board_data)

                            //for finding all the ids for every list names.
                            
                            let list_ids = Object.entries(lists).reduce((previous_state , list) => {
                                
                                list[1].forEach(list_information => {

                                    previous_state.push([list_information.name, list_information.id])

                                })

                                return previous_state

                            }, [])

                            callback3( list_ids, (err, card_list) =>{

                                if(err){

                                    final_callback(err, null)

                                }else{

                                    final_callback(null, card_list)

                                }

                            } )


                        }
                    } )
                }

            }  )


        }


    }, 2000)


}


module.exports = callback6