
const boards = require('./data/boards_1.json')

const lists = require('./data/lists_1.json')

const callback1 = require('./callback1.cjs')

const callback2 = require('./callback2.cjs')

const callback3 = require('./callback3.cjs')

function callback5( board_name, list_names, final_callback ){
    
    setTimeout(() => {

        let get_board_id = boards.filter( (board) => {
            
            if(board.name == board_name){

                return board;

            }

        } )

        /// For finding information about a board id of given board name.
        if(!get_board_id){

            final_callback("No board found with this name", null)

        }else{

            let board_id = get_board_id[0].id;

            callback1(board_id, (err, board_data) => {

                if(err){
                    final_callback(err, null)
                }
                else{
                    final_callback(null, board_data)

                    
                    // for finding all the lists of a board it
                    callback2( board_id, (err, board_data) => {
                        
                        if(err){

                            final_callback(err, null)

                        }else{

                            final_callback(null, board_data)

                            //for finding all the ids for given list names.

                            let list_ids = list_names.reduce((previous_state, each_list_name)=> {

                                Object.entries(lists).forEach(each_list_data => {

                                    each_list_data[1].forEach((card) => {

                                        if(card.name === each_list_name){
                                            
                                            previous_state.push([ card.name, card.id ])

                                        }
                                    })

                                   
                                })
                                return previous_state

                            }, [])
                            

                            callback3( list_ids, (err, card_list) =>{

                                if(err){

                                    final_callback(err, null)

                                }else{

                                    final_callback(null, card_list)

                                }

                            } )


                        }
                    } )
                }

            }  )


        }


    }, 2000)


}


module.exports = callback5