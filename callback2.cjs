
const lists = require('./data/lists_1.json')

function callback2(board_id, final_callback){

    setTimeout(() => {

        lists_belonging_to_a_board(lists, board_id, (err, list_of_a_board)=> {
    
            if(err){
    
                final_callback(err, null)
    
            }else{

                final_callback(null, list_of_a_board)
    
            }
    
        })

    }, 3000)

}

function lists_belonging_to_a_board(lists, given_board_id, callback_function){

    
    setTimeout( () => {
        
        try{
            let response = {}
    
            Object.entries(lists).forEach(([board_id, list]) => {
                
                if(board_id == given_board_id){
                    
                    response[board_id] = list
    
                }
        
            } )
            
                callback_function(null, response)
        }
        catch(err){
    
            callback_function(err, null)
    
        }
    
    }, 2000 )

}


module.exports = callback2