

const boards = require('./data/boards_1.json')

function callback1( board_id, final_callback ){

    setTimeout( () => {


        collect_information_from_boards_list(boards ,board_id, (err, board) => {
    
            if(err){
    
                final_callback(err, null)
    
            }
            else{
    
                final_callback(null, board)

            }
    
        })

    }, 3000 )

   

}


function collect_information_from_boards_list(boards, given_board_id, callback_function){

    
    setTimeout( () => {

        try{

            boards.forEach((board) => {

                if(board.id === given_board_id){

                    callback_function(null, board)

                }
        
            }  )
        }
        catch(err){
        
            callback_function(err, null)
        
        }
        
    }, 2000 )

}


module.exports = callback1
